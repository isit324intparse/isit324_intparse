using System;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine(((int?)1 ?? null) == 1 ? "pass" : "fail");
    }
}
