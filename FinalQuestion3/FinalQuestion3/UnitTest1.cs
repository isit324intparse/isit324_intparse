﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualBasic;

namespace FinalQuestion3
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod_Payment()
        {
            double rate = 0.05/12,
                term = 10*12,
                amount = 100000;

            double payment = Financial.Pmt(rate, term, -amount);

            payment = Math.Round(payment, 2);

            Assert.IsNotNull(payment);
            Assert.AreEqual(1060.66, payment);
        }
    }
}
