﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void FactorialButton(string num)
        {
            try
            {
                int n = int.Parse(num);

                long result = Factorial(n);
                labelResult.Text = "Result: " + result.ToString();
            }
            catch (Exception)
            {
                labelResult.Text = "Result: Error";
            }

        }

        private long Factorial(int n)
        {
            if (n < 0)
                throw new ArgumentOutOfRangeException();

            if (n == 0)
                return 1;

            long value = 1;
            for (int i = n; i > 0; i--)
            {
                value *= i;
            }
            return value;
        }

        private void buttonNeg1_Click(object sender, EventArgs e)
        {
            FactorialButton("-1");
        }

        private void button0_Click(object sender, EventArgs e)
        {
            FactorialButton("0");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FactorialButton("1");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FactorialButton("2");
        }

        private void button12_Click(object sender, EventArgs e)
        {
            FactorialButton("12");
        }
    }
}
