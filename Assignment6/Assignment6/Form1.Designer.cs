﻿namespace Assignment6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelResult = new System.Windows.Forms.Label();
            this.buttonNeg1 = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelResult
            // 
            this.labelResult.AccessibleDescription = "Result Label";
            this.labelResult.AccessibleName = "Result Label";
            this.labelResult.AutoSize = true;
            this.labelResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResult.Location = new System.Drawing.Point(27, 221);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(264, 83);
            this.labelResult.TabIndex = 7;
            this.labelResult.Text = "Result:";
            // 
            // buttonNeg1
            // 
            this.buttonNeg1.Location = new System.Drawing.Point(41, 63);
            this.buttonNeg1.Name = "buttonNeg1";
            this.buttonNeg1.Size = new System.Drawing.Size(75, 68);
            this.buttonNeg1.TabIndex = 11;
            this.buttonNeg1.Text = "-1";
            this.buttonNeg1.UseVisualStyleBackColor = true;
            this.buttonNeg1.Click += new System.EventHandler(this.buttonNeg1_Click);
            // 
            // button0
            // 
            this.button0.Location = new System.Drawing.Point(141, 63);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(75, 68);
            this.button0.TabIndex = 12;
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = true;
            this.button0.Click += new System.EventHandler(this.button0_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(441, 63);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 68);
            this.button12.TabIndex = 15;
            this.button12.Text = "12";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(341, 63);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 68);
            this.button2.TabIndex = 14;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(241, 63);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 68);
            this.button1.TabIndex = 13;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AccessibleDescription = "Factorial Form";
            this.AccessibleName = "Factorial";
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 440);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button0);
            this.Controls.Add(this.buttonNeg1);
            this.Controls.Add(this.labelResult);
            this.Name = "Form1";
            this.Text = "Factorial";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.Button buttonNeg1;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
    }
}

