﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;

namespace CalculateRate
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch stopWatch = new Stopwatch();
            long[] times = new long[5];

            double rate = 0.05 / 12,
                term = 10 * 12,
                amount = 100000;

            for (int i = 0; i < 5; i++)
            {
                stopWatch.Start();
                var payment = CalcRate(amount, term, rate);
                stopWatch.Stop();

                times[i] = stopWatch.ElapsedMilliseconds;
                stopWatch.Reset();

                Console.WriteLine("Payment {0}; RunTime: {1} ms", payment, times[i]);
            }

            Console.WriteLine("Average: " + times.Average());
            Console.ReadKey();
        }

        static double CalcRate(double amount, double term, double rate)
        { 
            double payment = Financial.Pmt(rate, term, -amount);

            return Math.Round(payment, 2);
        }
    }
}
