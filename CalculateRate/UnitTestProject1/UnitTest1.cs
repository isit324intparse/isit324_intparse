﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod_MortgageCalc()
        {
            Process p = new Process();

            p.StartInfo.FileName = @"..\..\..\CalculateRate\bin\Debug\CalculateRate.exe";
            p.StartInfo.Arguments = @"100000 10 5";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();
            string result = p.StandardOutput.ReadLine();

            Assert.AreEqual(result, "1060.66");
        }
    }
}
