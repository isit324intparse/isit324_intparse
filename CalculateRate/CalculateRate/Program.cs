﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;

namespace CalculateRate
{
    class Program
    {
        static void Main(string[] args)
        {
            double rate = Double.Parse(args[2]) / 100 / 12,
                term = Int32.Parse(args[1]) * 12,
                amount = Int32.Parse(args[0]);

            double payment = Financial.Pmt(rate, term, -amount);

            payment = Math.Round(payment, 2);

            Console.WriteLine(payment);
        }
    }
}
